package employee

class EmployeeDetails {

	String name;
	String sex;
	String mobile; 
	String dateOfJoin;
	String address;

	
    static constraints = {
	name(nullable: true)
	sex(nullable:true)
	sex(inList: ["M", "F"])
	mobile(nullable: true)
	dateOfJoin(nullable: true)
	address(nullable: true)
    }
}
