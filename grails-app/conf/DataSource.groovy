dataSource {
    pooled = true
    driverClassName = "org.postgresql.Driver"
}
hibernate {
    cache.use_second_level_cache=true
    cache.use_query_cache=true
    cache.provider_class='org.hibernate.cache.EhCacheProvider'
}
// environment specific settings
environments {
    development {
        dataSource {
            dbCreate = "update" // one of 'create', 'create-drop','update'
            url = "jdbc:postgresql://localhost:5432/grails"
	    username = "grails"
	    password = "grails"
        }
    }
    test {
        dataSource {
            dbCreate = "update"
            url = "jdbc:postgresql://localhost:5432/grails"
    		username = "grails"
    		password = "grails"
        }
    }
    production {
        dataSource {
            dbCreate = "update"
		dialect = org.hibernate.dialect.PostgreSQLDialect
            uri = new URI(System.env.DATABASE_URL?:"postgres://test:test@localhost/test")
		url = "jdbc:postgresql://"+uri.host+uri.path
		username = uri.userInfo.split(":")[0]
		password = uri.userInfo.split(":")[1]
        }
    }
}


/**
 * The top dataSource holds configuration options for ALL
 * environments... I'm presuming you want PostgreSQL in all
 * your environments but you may want to use the default
 * Hypersonic database in development and testing instead.
 */
/*dataSource {
    pooled = true
    driverClassName = "org.postgresql.Driver"
    // dialect = org.hibernate.dialect.PostgreSQLDialect
    dialect = net.sf.hibernate.dialect.PostgreSQLDialect
}
hibernate {
    cache.use_second_level_cache=true
    cache.use_query_cache=true
    cache.provider_class='com.opensymphony.oscache.hibernate.OSCacheProvider'
}
// environment specific settings
environments {
    development {
        dataSource {
            // one of 'create', 'create-drop','update'
            dbCreate = "create-drop"
            url="jdbc:postgresql://localhost:5432/dev"
            username = "dev"
            password = "dev"
        }
    }
    test {
        dataSource {
            dbCreate = "update"
            url="jdbc:postgresql://localhost:5432/test"
            driverClassName = "org.postgresql.Driver"
            username = "tester"
            password = "tester"
        }
    }
    production {
        dataSource {
            dbCreate = "update"
            url="jdbc:postgresql://localhost:5432/grails"
            username = "grails"
            password = "grails"
        }
    }
}*/

/*dataSource {
    pooled = true
    dbCreate = "create-drop" // one of 'create', 'create-drop','update'
    url = "jdbc:postgresql://localhost:5432/grails"
    driverClassName = "org.postgresql.Driver"
    username = "grails"
    password = "grails"
//    driverClassName = "org.h2.Driver"
//    username = "sa"
//    password = ""
}
hibernate {
    cache.use_second_level_cache = true
    cache.use_query_cache = false
    cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory' // Hibernate 3
//    cache.region.factory_class = 'org.hibernate.cache.ehcache.EhCacheRegionFactory' // Hibernate 4
}

// environment specific settings
environments {
    development {
        dataSource {
            dbCreate = "create-drop" // one of 'create', 'create-drop', 'update', 'validate', ''
            url = "jdbc:h2:mem:devDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
        }
    }
    test {
        dataSource {
            dbCreate = "update"
            url = "jdbc:h2:mem:testDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
        }
    }
    production {
        dataSource {
            dbCreate = "update"
            url = "jdbc:h2:prodDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
            properties {
               maxActive = -1
               minEvictableIdleTimeMillis=1800000
               timeBetweenEvictionRunsMillis=1800000
               numTestsPerEvictionRun=3
               testOnBorrow=true
               testWhileIdle=true
               testOnReturn=false
               validationQuery="SELECT 1"
               jdbcInterceptors="ConnectionState"
            }
        }
    }
}*/
